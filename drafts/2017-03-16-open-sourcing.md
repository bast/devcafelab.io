---
title: Background documentation for decisions about the open source transition of the Dalton/LSDalton code
published: 2017-03-16
author: Radovan Bast
---

# Authors

Radovan Bast and Roberto Di Remigio


# Executive summary

This document presents options for decisions about the open source transition
of the Dalton/LSDalton code and analyses these options according to strengths,
weaknesses, risks, and opportunities.

It is essential to note and understand that with Git by design it is not
possible to serve public and private branches in one single repository. This
means that by making one or more branches public, we need at least two
repositories.

Four separate and numbered decision items are discussed in this document:

- 1) Decision about the repository hosting platform
- 2) Decision about which branches should be part of the public repository.
- 3) Decision about the timeline for opening the public repository
- 4) Decision about the contribution model for the public repository


# Definitions

- Public repository: repository that is world-readable and can be cloned anonymously
- Private repository: only a restricted set of persons can clone the repository
- Public branch: branch that is world-readable
- Private branch: only a restricted set of persons can see and read such a branch


# 1) Decision about the repository hosting platform

## Options

- Option 1.1: [GitHub](https://github.com)
- Option 1.2: [GitLab.com](https://gitlab.com)
- Option 1.3: Nordic-wide GitLab server deployed by [NeIC](https://neic.nordforsk.org)


## Recommendation

- Write me ...


## Background

Currently the code is hosted on [GitLab.com](https://gitlab.com) (GitLab.com is
a service and company, GitLab is a software) A repository hosting platform not
only hosts the Git repository but also provides an issue tracker, user access
management, wiki, hooks and integrations with other services, as well as a
solution for code review. The choice of a repository hosting platform can
affect the workflow.


## Option 1.1: [GitHub](https://github.com)

### Strengths

- The currently largest repository hosting platform with millions of active projects and users
- Stability
- High availability
- Responsiveness
- Visibility
- Integration with [Travis CI](https://travis-ci.org) for automated tests


### Weaknesses

- Underlying source code powering the [GitHub](https://github.com) platform is not open source
- Private repositories cost money


### Risks

- Many Dalton developers may not be familiar with it (but the workflow is very similar to [GitLab.com](https://gitlab.com))
- Lock-in through integrations and issue tracking and code review discussions


### Opportunities

- Clean-slate opportunity to prevent direct pushes to the main development line and to implement code review from day 1
- Forks of the public repository are more likely to be public


## Option 1.2: [GitLab.com](https://gitlab.com)

### Strengths

- Private repositories are free
- Underlying source code powering the community edition of [GitLab](https://gitlab.com) is open source
- Dalton developers are familiar with the platform
- More flexibility to connect repositories with own test runners


### Weaknesses

- Slightly less responsive and available than [GitHub](https://github.com)
- Less visibility than [GitHub](https://github.com)


### Risks

- High inertia to implement code review and prevent direct pushed to the main development line
- Forks of the public repository may remain private


### Opportunities

- Easier to migrate data to an own instance


## Option 1.3: Nordic-wide GitLab server deployed by [NeIC](https://neic.nordforsk.org)

### Strengths

- Full control over data
- Data stays within the Nordics
- Commitment to a service


### Weaknesses

- Not in place yet
- Will probably be available as beta in April or May
- Less availability (much smaller support team) than Option 1.2


### Risks

- Long-term future is not clear but NeIC is workin on a long-term sustainability of this service


### Opportunities

- Politically good solution to reinforce NeIC initiatives and services


# 2) Decision about which branches should be part of the public repository


## Options

- Option 2.1: We only open the master branch and past release branches
- Option 2.2: We open all branches by default but give developers enough time to mark those branches which should not be opened


## Recommendation

- Write me ...


## Background

In Dalton we have 236 branches. Most of these are unmaintained. In LSDalton we
have 81 branches. We will open the entire history of at least the `master`
branch and past release branches but we can also open additional branches.


## Option 2.1: We only open the master branch and past release branches

### Strengths

- Simple, clear, and clean


### Weaknesses

- ?


### Risks

- Since the public repository is supposed to be the main repository we risk
  leaving many branches behind
- Some developers may regard the public repository as a mirror and continue
  contributing to the `master` branch on the private repo which would let both
  repositories diverge


### Opportunities

- Getting rid of many unmaintained branches


## Option 2.2: We open all branches by default but give developers enough time to mark those branches which should not be opened

### Strengths

- No unmaintained branch left behind


### Weaknesses

- Main repository cluttered with tens and hundreds of branches


### Risks

- Some developers may forget branches and accidentally open them


### Opportunities

- Less risk that the public repository is not accepted as the main development line


# 3) Decision about the timeline for opening the public repository

## Options

- Option 3.1: Make the public repository available as soon as technically possible
- Option 3.2: Make the public repository available as soon as we release a new Dalton/LSDalton version


## Recommendation

- Write me ...


## Background

As soon as we make the public repository available, users can start cloning the
code and using the `master` branch or any of the release branches and
developers can start building based on our code by forking the repository. The
opening of the repository is in principle independent of an official Dalton
release. The opening can be synchronized with a new official release of the
code. In any outcome we will make explicitly clear where the released versions
reside, how to reproduce them (they may require a "make release" step) and that
the `master` branch may contain experimental and untested and unreleased
functionality.


## Option 3.1: Make the public repository available as soon as technically possible

### Strengths

- The opening does not get delayed if the release gets delayed
- Transparent release preparation


### Weaknesses

- Requires clear documentation and communication of scope and intent to avoid confusion


### Risks

- Users may misunderstand this step as releasing a new version


### Opportunities

- We open the code sooner and reduce the "time to market"
- Users have the chance to report issues during release preparation


## Option 3.2: Make the public repository available as soon as we release a new Dalton/LSDalton version

### Strengths

- ?


### Weaknesses

- Non-transparent release preparation


### Risks

- Opening risks to get delayed with a delayed release
- No opportunity to report issues during release preparation


### Opportunities

- ?


# 4) Decision about the contribution model for the public repository


## Options

- Option 4.1: All developers can push (present model)
- Option 4.2: Nobody pushes directly, all contributions are integrated via pull-requests or merge-requests


## Recommendation

- Write me ...


## Background

Write me ...
